use csv::StringRecord;
use std::fmt;

// This record is optional and holds any messages or notes that need to be
// associated with a job, for example customer details, special instructions,
// etc, or any details that are job related rather than part or material
// related. As many lines as required can be stored for each job.
#[derive(Default, Debug, Clone)]
pub struct Notes {
    // index linking note to job
    pub job_index: i32,
    // index storing order of notes
    pub notes_index: i32,
    // Text of Note Maximum length of text field is 250 characters.
    pub text: String,
}

impl Notes {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "NOTES" => Notes {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                notes_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                text: r.next().unwrap().trim().to_string(),
            },
            _ => panic!("Not a notes record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.notes_index.to_string(),
            self.text.to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Notes {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Note - Job IDX: {} - Note IDX: {}\nText: {}",
               self.job_index,
               self.notes_index,
               self.text,
        )
    }
}
