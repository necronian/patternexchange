use csv::StringRecord;
use std::fmt;

// This optional record contains standard information about each different
// size (or line item) in the cutting list. One use of this record is to hold
// data for label printing.
#[derive(Default, Debug, Clone)]
pub struct PartsInf {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Index number linking this record with other part records.
    pub part_index: i32,
    // A second part description
    pub desc: Option<String>,
    // Number of copies of the label for this part. 0 = no labels for this
    // part default if not specified =1
    pub label_qty: Option<String>,
    // Length of part after edging and trimming
    pub fin_length: Option<String>,
    // Width of part after edging and trimming
    pub fin_width: Option<String>,
    // Original order/job/work number which part relates to
    pub order: Option<String>,
    // Code or description of edging for bottom (length) edge
    pub edge1: Option<String>,
    // Code or description of edging for top (length) edge
    pub edge2: Option<String>,
    // Code or description of edging for left (width) edge
    pub edge3: Option<String>,
    // Code or description of edging for right (width) edge
    pub edge4: Option<String>,
    // Program or operation code for bottom (length) edge
    pub edg_pg1: Option<String>,
    // Program or operation code for top (length) edge
    pub edg_pg2: Option<String>,
    // Program or operation code left (width) edge
    pub edg_pg3: Option<String>,
    // Program or operation code for right (width) edge
    pub edg_pg4: Option<String>,
    // Code/description of laminate material for face (topside) of part
    pub face_lam: Option<String>,
    // Code /description of laminate material for back (underside) of part
    pub back_lam: Option<String>,
    // Code or description of core material
    pub core_mat: Option<String>,
    // Pallet layout (stacks in length and width)
    pub pallet: Option<String>,
    // Name of drawing file, drill program or CNC program for machine centre
    pub drawing: Option<String>,
    // Product or cabinet code or template name to which part belongs
    pub product: Option<String>,
    // Description of product or cabinet
    pub prod_info: Option<String>,
    // External dimension of product or cabinet
    pub prod_width: Option<String>,
    // External dimension of product or cabinet
    pub prod_hgt: Option<String>,
    // External dimension of product or cabinet
    pub prod_depth: Option<String>,
    // Item number of cabinet in room
    pub prod_num: Option<String>,
    // Room or floor or group item number for cabinet
    pub room: Option<String>,
    // data for 1st barcode – as text string
    pub barcode1: Option<String>,
    // data for second bar code – as text string
    pub barcode2: Option<String>,
    // The extended colour name.
    pub colour: Option<String>,
    // Length of part prior to second cut
    pub second_cut_length: Option<String>,
    // Width of part prior to second cut
    pub second_cut_width: Option<String>,
}

impl PartsInf {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "PARTS_INF" => PartsInf {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                part_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                desc: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                label_qty: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                fin_length: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                fin_width: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                order: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edge1: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edge2: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edge3: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edge4: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edg_pg1: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edg_pg2: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edg_pg3: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                edg_pg4: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                face_lam: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                back_lam: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                core_mat: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                pallet: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                drawing: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                product: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                prod_info: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                prod_width: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                prod_hgt: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                prod_depth: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                prod_num: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                room: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                barcode1: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                barcode2: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                colour: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                second_cut_length: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                second_cut_width: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
            },
            _ => panic!("Not a parts_inf record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.part_index.to_string(),
            if let Some(data) = &self.desc { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.label_qty { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.fin_length { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.fin_width { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.order { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edge1 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edge2 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edge3 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edge4 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edg_pg1 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edg_pg2 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edg_pg3 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.edg_pg4 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.face_lam { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.back_lam { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.core_mat { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.pallet { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.drawing { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.product { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.prod_info { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.prod_width { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.prod_hgt { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.prod_depth { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.prod_num { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.room { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.barcode1 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.barcode2 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.colour { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.second_cut_length { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.second_cut_width { data.to_string() } else { "".to_string() },
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for PartsInf {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Part Info - Job IDX: {} - Part IDX: {} - Barcode: {} - Barcode 2: {}\n\
                Desc: {}\n\
                Finished Size: {} x {} (LxW) - Order: {} - Labels: {}\n\
                Edges: {} x {} x {} x {} - Edges: {} x {} x {} x {}\n\
                Laminate Top/Bot: {} / {}\n\
                Core: {}\n\
                Pallet: {} - Drawing: {}\n\
                Product: {} - Product Info: {}\n\
                Product WxHxD - {} x {} x {} - Product Number: {}\n\
                Room: {} - Color: {}\n\
                Second Cut LxW: {} x {}",
               self.job_index,
               self.part_index,
               self.barcode1.as_ref().unwrap_or(&"".to_string()),
               self.barcode2.as_ref().unwrap_or(&"".to_string()),
               self.desc.as_ref().unwrap_or(&"".to_string()),
               self.fin_length.as_ref().unwrap_or(&"".to_string()),
               self.fin_width.as_ref().unwrap_or(&"".to_string()),
               self.order.as_ref().unwrap_or(&"".to_string()),
               self.label_qty.as_ref().unwrap_or(&"".to_string()),
               self.edge1.as_ref().unwrap_or(&"".to_string()),
               self.edge2.as_ref().unwrap_or(&"".to_string()),
               self.edge3.as_ref().unwrap_or(&"".to_string()),
               self.edge4.as_ref().unwrap_or(&"".to_string()),
               self.edg_pg1.as_ref().unwrap_or(&"".to_string()),
               self.edg_pg2.as_ref().unwrap_or(&"".to_string()),
               self.edg_pg3.as_ref().unwrap_or(&"".to_string()),
               self.edg_pg4.as_ref().unwrap_or(&"".to_string()),
               self.face_lam.as_ref().unwrap_or(&"".to_string()),
               self.back_lam.as_ref().unwrap_or(&"".to_string()),
               self.core_mat.as_ref().unwrap_or(&"".to_string()),
               self.pallet.as_ref().unwrap_or(&"".to_string()),
               self.drawing.as_ref().unwrap_or(&"".to_string()),
               self.product.as_ref().unwrap_or(&"".to_string()),
               self.prod_info.as_ref().unwrap_or(&"".to_string()),
               self.prod_width.as_ref().unwrap_or(&"".to_string()),
               self.prod_hgt.as_ref().unwrap_or(&"".to_string()),
               self.prod_depth.as_ref().unwrap_or(&"".to_string()),
               self.prod_num.as_ref().unwrap_or(&"".to_string()),
               self.room.as_ref().unwrap_or(&"".to_string()),
               self.colour.as_ref().unwrap_or(&"".to_string()),
               self.second_cut_length.as_ref().unwrap_or(&"".to_string()),
               self.second_cut_width.as_ref().unwrap_or(&"".to_string()),
        )
    }
}
