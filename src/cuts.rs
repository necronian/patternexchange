use csv::StringRecord;
use std::fmt;

// These records define each cut for the saw and determine the parts produced
// by each cut. This is used, for example, so that the correct labels can be
// printed at the saw in synchronisation with the cutting.
//
// Some points about the cut record
// --------------------------------
// Some cuts produce several parts with different item numbers because
// although the parts may have the same size they will be labeled
// uniquely. This occurs when cutting multiple sheets in a book where the
// parts on different sheets have different item numbers. These duplicate
// parts are represented with dummy CUTS records showing the part index and
// part quantity but a zero dimension and zero cut quantity. When cutting
// exact fit patterns (e.g. no trims, strip fits exactly in length of the
// board) some cuts will produce two parts side by side (e.g. the last cross
// cut in a strip). If this is the case the cut quantity of the last part will
// be set to zero, the dimension remains unchanged. Note that it is important
// that these records have a dimension so as to differentiate them from the
// dummy cut records for duplicate parts. The Sequence number allows
// definition of different parts in a stack produced from the same cut. Note,
// the cuts are not listed in cut sequence because it is necessary to nest the
// 2 nd , 3 rd and later phase cuts. The SEQUENCE number is optional, and if
// not given then the cutting sequence should be determined by the saw or a
// post-processor.
// 
// The PART_INDEX (if not 0) points to the PARTS_REQ records of relevant part
// or ‘X’ + OFC_INDEX in OFFCUTS records.
// 
// The QTY_PARTS field allows for the display of the correct part quantities
// for duplicate parts. In a pattern with run quantity 20, and cut 6 sheets at
// a time, then there will be 4 cycles or books (3 with 6 sheets and 1 with 2
// sheets). Say the first part in the top left corner is a mixture of 14 parts
// item 1, and 6 parts item 2. The first book at the appropriate cut will
// produce quantity 6 labels of item 1, the second book also 6 of 1, the third
// book will produce 2 labels of item1 and 4 of item 2, and the last book will
// give 2 of item 2. In this example, the CUTS records would show two cut
// lines, item 1 quantity 14, and item 2 quantity 6. The saw takes care of
// counting the cycles.
//     
// Note that the Sequence number will increment by the repeat quantity for
// that cut. In example below, CUTS 1,1,2 has repeat 3, indicating three cuts,
// so sequence number 4 implies 4/5/6 and the next sequence number is
// incremented by 3.
#[derive(Default, Debug, Clone)]
pub struct Cuts {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Index number used to link this record with pattern records
    pub ptn_index: i32,
    // Sequential index number starting at 1 for each new pattern and
    // incrementing by 1 for each cut
    pub cut_index: i32,
    // Cut sequence number indicating order in which cuts are processed by saw
    pub sequence: i32,
    // The type of cut:
    // 0 = head cut
    // 1 = rip cut
    // 2 = cross cut
    // 3 = 3 rd phase / recut
    // 4 = 4 th phase /recut
    // Maximum phase = 9
    // 90,91,92,93 = trim / waste cut corresponding to phase of cut (to
    // override defaults)
    pub function: i32,
    // The size of cut in measurement units
    pub dimension: f64,
    // The repeat quantity for this cut
    pub qty_rpt: i32,
    // 0 if no part produced or part index number in part or offcut records
    pub part_index: String,
    // Quantity of this part produced by this cut for all cycles of this
    // pattern.
    pub qty_parts: i32,
    // optional field to store narrative about the cut instruction
    pub comment: Option<String>,
}

impl Cuts {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "CUTS" => Cuts {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                ptn_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                cut_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                sequence: r.next().unwrap().trim().parse::<i32>().unwrap(),
                function: r.next().unwrap().trim().parse::<i32>().unwrap(),
                dimension: r.next().unwrap().trim().parse::<f64>().unwrap(),
                qty_rpt: r.next().unwrap().trim().parse::<i32>().unwrap(),
                part_index: r.next().unwrap().trim().to_string(),
                qty_parts: r.next().unwrap().trim().parse::<i32>().unwrap(),
                comment: r.next().map(|x| Some(x.trim().to_string())).unwrap_or(None),
            },
            _ => panic!("Not a cuts record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.ptn_index.to_string(),
            self.cut_index.to_string(),
            self.sequence.to_string(),
            self.function.to_string(),
            self.dimension.to_string(),
            self.qty_rpt.to_string(),
            self.part_index.to_string(),
            self.qty_parts.to_string(),
            self.comment.as_ref().unwrap_or(&"".to_string()).to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Cuts{
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Cut - Job IDX: {} - Pattern IDX: {} - Cut IDX: {} - Part IDX: {}\n\
                Sequence: {} - Function: {}\n\
                Dimension: {} - Repeat: {} - Parts: {}\n\
                Comment: {}",
               self.job_index,
               self.ptn_index,
               self.cut_index,
               self.part_index,
               self.sequence,
               match self.function {
                   0 => "Head Cut".to_string(),
                   1 => "Rip Cut".to_string(),
                   2 => "Cross Cut".to_string(),
                   3 => "3rd Cut".to_string(),
                   n @ 4..=9  => format!("{}th Cut",n),
                   n @ 90..=99 => format!("{}th Cut for Waste/Trim",n),
                   _ => "Unknown".to_string(),
               },
               self.dimension,
               self.qty_rpt,
               self.qty_parts,
               if let Some(comment) = &self.comment { comment.to_string() } else { "".to_string() },
        )
    }
}
