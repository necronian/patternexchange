use csv::StringRecord;
use std::fmt;

// This record contains data about each different size (or line item) in the
// cutting list. This record is used to provide details about each part (over
// and above cut sizes).
#[derive(Default, Debug, Clone)]
pub struct PartsReq {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Index number to link this record with other associated part records
    pub part_index: i32,
    // Part code or description.
    pub code: String,
    // Index of material used for this part.
    pub mat_index: i32,
    // Cut length of part shown in appropriate measurement unit
    pub length: f64,
    // Cut length of part shown in appropriate measurement unit
    pub width: f64,
    // number of pieces this size
    pub qty_req: i32,
    // allowed over production
    pub qty_over: i32,
    // allowed under production.
    pub qty_under: i32,
    // 0 = No grain/part can be rotated
    // 1 = grain along the length of the board/part cannot be rotated
    // 2 = grain along the width of the board/part must be rotated
    pub grain: i32,
    // quantity of parts produced by patterns
    pub qty_prod: Option<i32>,
    // quantity of parts not produced because of an error
    pub under_prod_error: Option<i32>,
    // quantity of parts not produced because of allowed under production
    pub under_prod_allowed: Option<i32>,
    // quantity of plus parts not produced
    pub under_prod_pluspart: Option<i32>,
}

impl PartsReq {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "PARTS_REQ" => PartsReq {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                part_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                code: r.next().unwrap().trim().to_string(),
                mat_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                length: r.next().unwrap().trim().parse::<f64>().unwrap(),
                width: r.next().unwrap().trim().parse::<f64>().unwrap(),
                qty_req: r.next().unwrap().trim().parse::<i32>().unwrap(),
                qty_over: r.next().unwrap().trim().parse::<i32>().unwrap(),
                qty_under: r.next().unwrap().trim().parse::<i32>().unwrap(),
                grain: r.next().unwrap().trim().parse::<i32>().unwrap(),
                qty_prod: r.next()
                    .map(|x| Some(x.trim().parse::<i32>().unwrap()))
                    .unwrap_or(None),
                under_prod_error: r.next()
                    .map(|x| Some(x.trim().parse::<i32>().unwrap()))
                    .unwrap_or(None),
                under_prod_allowed: r.next()
                    .map(|x| Some(x.trim().parse::<i32>().unwrap()))
                    .unwrap_or(None),
                under_prod_pluspart: r.next()
                    .map(|x| Some(x.trim().parse::<i32>().unwrap()))
                    .unwrap_or(None),
            },
            _ => panic!("Not a parts_req record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.part_index.to_string(),
            self.code.to_string(),
            self.mat_index.to_string(),
            self.length.to_string(),
            self.width.to_string(),
            self.qty_req.to_string(),
            self.qty_over.to_string(),
            self.qty_under.to_string(),
            self.grain.to_string(),
            if let Some(data) = self.qty_prod { data.to_string() } else { "".to_string() },
            if let Some(data) = self.under_prod_error { data.to_string() } else { "".to_string() },
            if let Some(data) = self.under_prod_allowed { data.to_string() } else { "".to_string() },
            if let Some(data) = self.under_prod_pluspart { data.to_string() } else { "".to_string() },
        ];

        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for PartsReq {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "PART: Job IDX: {} - Part IDX: {} - Code: {} - Material IDX: {}\n {} x {} (LxW) - Quantity {} - Grain: {} - Over/Under: {}/{}\nProduced Quantity: {} - Under Err: {} - Under Allowed: {} - Total Missing: {}",
               self.job_index,
               self.part_index,
               self.code,
               self.mat_index,
               self.length,
               self.width,
               self.qty_req,
               if self.grain == 0 { "None" }
               else if self.grain == 1 { "Grained" }
               else if self.grain == 2 { "Cross Grained" }
               else { "Unknown" },
               self.qty_over,
               self.qty_under,
               self.qty_prod.unwrap(),
               self.under_prod_error.unwrap(),
               self.under_prod_allowed.unwrap(),
               self.under_prod_pluspart.unwrap(),
        )
    }
}
