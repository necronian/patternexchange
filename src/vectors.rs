use csv::StringRecord;
use std::fmt;

// This table holds an optional description of the pattern as a set of vectors.
//
// The origin of the drawing is defined in the HEADER record. The x and y
// positions specify the distance to include the saw kerf (saw blade
// thickness), away from origin. So, a 10 mm trim would result in a vector at
// x=10, where if saw kerf (saw blade thickness) is 4.5, then waste removed is
// 5.5. The position for cuts producing parts must include all saw kerfs. Note
// that unlike the CUT records where all dimensions are relative, in these
// records all dimensions are absolute values.
#[derive(Default, Debug, Clone)]
pub struct Vectors {
    // Index number used to link this record to other records for this job
    pub job_index: i32,
    // This is an index number used to link this to the PATTERN record
    pub ptn_index: i32,
    // This is an index number to relate the vector to the CUT record
    pub cut_index: i32,
    // Start co-ordinate of cut in X (always positive)
    pub x_start: f64,
    // Start co-ordinate of cut in Y (always positive)
    pub y_start: f64,
    // End co-ordinate of cut in X (always positive)
    pub x_end: f64,
    // End co-ordinate of cut in Y (always positive)
    pub y_end: f64,
}

impl Vectors {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "VECTORS" => Vectors {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                ptn_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                cut_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                x_start: r.next().unwrap().trim().parse::<f64>().unwrap(),
                y_start: r.next().unwrap().trim().parse::<f64>().unwrap(),
                x_end: r.next().unwrap().trim().parse::<f64>().unwrap(),
                y_end: r.next().unwrap().trim().parse::<f64>().unwrap(),
            },
            _ => panic!("Not a vectors record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.ptn_index.to_string(),
            self.cut_index.to_string(),
            self.x_start.to_string(),
            self.y_start.to_string(),
            self.x_end.to_string(),
            self.y_end.to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Vectors {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "",
        )
    }
}
