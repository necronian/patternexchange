use clap::Parser;
use patternexchange::Ptx;
use std::error::Error;
use tiberius::{Client, Config, AuthMethod};
use tokio::net::TcpStream;
use tokio_util::compat::TokioAsyncWriteCompatExt;

#[derive(Parser, Debug)]
#[clap(version, author, about = "Import PTX files to SQL Server.")]
struct Args {
    #[clap(help = "The SQL Server host.", default_value = "192.168.1.10" , short, long)]
    host: String,
    #[clap(help = "The SQL Server port.", default_value_t = 50752, long)]
    port: u16,
    #[clap(help = "The SQL Server username.", default_value = "IMOSADMIN" , short, long)]
    username: String,
    #[clap(help = "The SQL Server password.", default_value = "imos" , short, long)]
    password: String,
    #[clap(help = "The SQL Server database.", default_value = "ArnoldIMOSAdjacent" , short, long)]
    database: String,
    file: String,
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    // Tiberius SQL configuration to pass to database functions
    let mut sql_config = Config::new();
    sql_config.host(args.host);
    sql_config.port(args.port);
    sql_config.database(args.database);
    sql_config.encryption(tiberius::EncryptionLevel::NotSupported);
    sql_config.authentication(AuthMethod::sql_server(args.username, args.password));
    
    let file = args.file;

    let data = Ptx::parse(&file);

    insert_ptx(&file,&data,sql_config).await?;
    
    Ok(())
}

pub async fn insert_ptx(file: &str, ptx: &Ptx, config: Config) -> Result<(), Box<dyn Error>> {
    let tcp = TcpStream::connect(config.get_addr()).await?;
    tcp.set_nodelay(true)?;
    let mut client = Client::connect(config, tcp.compat_write()).await?;

    // INSERT MAIN PTX RECORD
    let ptx_data = client.query("INSERT INTO [dbo].[PTX] ([FILENAME]) \
                                 OUTPUT ([INSERTED].[ID]) \
                                 VALUES (@P1)", &[&file]).await?;
    let row = ptx_data.into_row().await?.unwrap();
    let id: i32 = row.get("ID").expect("ID Not Returned from DB When Inserting PTX");

    // INSERT HEADER
    client.execute("INSERT INTO [dbo].[PTX_HEADER] \
                    ([ID], [VERSION], [TITLE], [UNITS], [ORIGIN], [TRIM_TYPE]) \
                    VALUES (@P1,@P2,@P3,@P4,@P5,@P6)",
                   &[&id,
                     &ptx.header.version,
                     &ptx.header.title,
                     &ptx.header.units,
                     &ptx.header.origin,
                     &ptx.header.trim_type]).await?;

    //INSERT JOBS
    if let Some(jobs) = &ptx.jobs {
        for job in jobs {
            client.execute("INSERT INTO [dbo].[PTX_JOBS] \
                            ([ID], [JOB_INDEX], [NAME], [DESC], [ORD_DATE], \
                            [CUT_DATE], [CUSTOMER], [STATUS], [OPT_PARAM], \
                            [SAW_PARAM], [CUT_TIME], [WASTE_PCNT]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,@P12)",
                           &[&id,
                             &job.job_index,
                             &job.name,
                             &job.desc,
                             &job.ord_date,
                             &job.cut_date,
                             &job.customer,
                             &job.status,
                             &job.opt_param,
                             &job.saw_param,
                             &job.cut_time,
                             &job.waste_pcnt]).await?;
        }
    }

    //INSERT PARTS_REQ
    for data in &ptx.parts_req {
        client.execute("INSERT INTO [dbo].[PTX_PARTS_REQ] \
                        ([ID], [JOB_INDEX], [PART_INDEX], \
                        [CODE], [MAT_INDEX], [LENGTH], [WIDTH], \
                        [QTY_REQ], [QTY_OVER], [QTY_UNDER], [GRAIN], \
                        [QTY_PROD], [UNDER_PROD_ERROR], [UNDER_PROD_ALLOWED], \
                        [UNDER_PROD_PLUSPART]) \
                        VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,@P12,\
                        @P13,@P14,@P15)",
                       &[&id,
                         &data.job_index,
                         &data.part_index,
                         &data.code,
                         &data.mat_index,
                         &data.length,
                         &data.width,
                         &data.qty_req,
                         &data.qty_over,
                         &data.qty_under,
                         &data.grain,
                         &data.qty_prod,
                         &data.under_prod_error,
                         &data.under_prod_allowed,
                         &data.under_prod_pluspart]).await?;
    }

    //INSERT PARTS_INF
    if let Some(vector) = &ptx.parts_inf {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_PARTS_INF] \
                            ([ID], \
                            [JOB_INDEX], [PART_INDEX], [DESC], [LABEL_QTY], \
                            [FIN_LENGTH], [FIN_WIDTH], [ORDER], [EDGE1], [EDGE2], \
                            [EDGE3], [EDGE4], [EDG_PG1], [EDG_PG2], [EDG_PG3], \
                            [EDG_PG4], [FACE_LAM], [BACK_LAM], [CORE_MAT], [PALLET], \
                            [DRAWING], [PRODUCT], [PROD_INFO], [PROD_WIDTH], \
                            [PROD_HGT], [PROD_DEPTH], [PROD_NUM], [ROOM], [BARCODE1], \
                            [BARCODE2], [COLOUR], [SECOND_CUT_LENGTH], \
                            [SECOND_CUT_WIDTH] ) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,\
                            @P12,@P13,@P14,@P15,@P16,@P17,@P18,@P19,@P20,@P21,@P22,\
                            @P23,@P24,@P25,@P26,@P27,@P28,@P29,@P30,@P31,@P32,@P33)",
                           &[&id,
                             &data.job_index,
                             &data.part_index,
                             &data.desc,
                             &data.label_qty,
                             &data.fin_length,
                             &data.fin_width,
                             &data.order,
                             &data.edge1,
                             &data.edge2,
                             &data.edge3,
                             &data.edge4,
                             &data.edg_pg1,
                             &data.edg_pg2,
                             &data.edg_pg3,
                             &data.edg_pg4,
                             &data.face_lam,
                             &data.back_lam,
                             &data.core_mat,
                             &data.pallet,
                             &data.drawing,
                             &data.product,
                             &data.prod_info,
                             &data.prod_width,
                             &data.prod_hgt,
                             &data.prod_depth,
                             &data.prod_num,
                             &data.room,
                             &data.barcode1,
                             &data.barcode2,
                             &data.colour,
                             &data.second_cut_length,
                             &data.second_cut_width]).await?;
        }
    }

    //INSERT PARTS_UDI
    if let Some(vector) = &ptx.parts_udi {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_PARTS_UDI] \
                            ([ID], [JOB_INDEX], [PART_INDEX], \
                            [INFO01], [INFO02], [INFO03], [INFO04], [INFO05], \
                            [INFO06], [INFO07], [INFO08], [INFO09], [INFO10], \
                            [INFO11], [INFO12], [INFO13], [INFO14], [INFO15], \
                            [INFO16], [INFO17], [INFO18], [INFO19], [INFO20], \
                            [INFO21], [INFO22], [INFO23], [INFO24], [INFO25], \
                            [INFO26], [INFO27], [INFO28], [INFO29], [INFO30], \
                            [INFO31], [INFO32], [INFO33], [INFO34], [INFO35], \
                            [INFO36], [INFO37], [INFO38], [INFO39], [INFO40], \
                            [INFO41], [INFO42], [INFO43], [INFO44], [INFO45], \
                            [INFO46], [INFO47], [INFO48], [INFO49], [INFO50], \
                            [INFO51], [INFO52], [INFO53], [INFO54], [INFO55], \
                            [INFO56], [INFO57], [INFO58], [INFO59], [INFO60] ) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,\
                            @P12,@P13,@P14,@P15,@P16,@P17,@P18,@P19,@P20,@P21,@P22,\
                            @P23,@P24,@P25,@P26,@P27,@P28,@P29,@P30,@P31,@P32,@P33,\
                            @P34,@P35,@P36,@P37,@P38,@P39,@P40,@P41,@P42,@P43,@P44,\
                            @P45,@P46,@P47,@P48,@P49,@P50,@P51,@P52,@P53,@P54,@P55,\
                            @P56,@P57,@P58,@P59,@P60,@P61,@P62,@P63)",
                           &[&id,
                             &data.job_index,
                             &data.part_index,
                             &data.info01,
                             &data.info02,
                             &data.info03,
                             &data.info04,
                             &data.info05,
                             &data.info06,
                             &data.info07,
                             &data.info08,
                             &data.info09,
                             &data.info10,
                             &data.info11,
                             &data.info12,
                             &data.info13,
                             &data.info14,
                             &data.info15,
                             &data.info16,
                             &data.info17,
                             &data.info18,
                             &data.info19,
                             &data.info20,
                             &data.info21,
                             &data.info22,
                             &data.info23,
                             &data.info24,
                             &data.info25,
                             &data.info26,
                             &data.info27,
                             &data.info28,
                             &data.info29,
                             &data.info30,
                             &data.info31,
                             &data.info32,
                             &data.info33,
                             &data.info34,
                             &data.info35,
                             &data.info36,
                             &data.info37,
                             &data.info38,
                             &data.info39,
                             &data.info40,
                             &data.info41,
                             &data.info42,
                             &data.info43,
                             &data.info44,
                             &data.info45,
                             &data.info46,
                             &data.info47,
                             &data.info48,
                             &data.info49,
                             &data.info50,
                             &data.info51,
                             &data.info52,
                             &data.info53,
                             &data.info54,
                             &data.info55,
                             &data.info56,
                             &data.info57,
                             &data.info58,
                             &data.info59,
                             &data.info60]).await?;
        }
    }

    //INSERT PARTS_DST
    if let Some(vector) = &ptx.parts_dst {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_PARTS_DST] \
                            ([ID], [JOB_INDEX], [PART_INDEX], \
                            [PART_LAY_L], [PART_LAY_W], [PART_LAY_O], \
                            [STK_HGHT_Q], [STK_HGHT_D], [STATION], [QTY_STACKS], \
                            [BTM_TYPE], [BTM_DESC], [BTM_MATL], [BTM_LENGTH], \
                            [BTM_WIDTH], [BTM_THICK], [OVER_LEN], [OVER_WID], \
                            [BTM_LAY_L], [BTM_LAY_W], [TOP_TYPE], [TOP_DESC], \
                            [TOP_MATL], [TOP_LENGTH], [TOP_WIDTH], [TOP_THICK], \
                            [TOP_LAY_L], [TOP_LAY_W], [SUP_TYPE], [SUP_DESC], \
                            [SUP_MATL], [SUP_LENGTH], [SUP_WIDTH], [SUP_THICK], \
                            [SUP_LAY_L], [SUP_LAY_W], [STATION2]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,\
                            @P12,@P13,@P14,@P15,@P16,@P17,@P18,@P19,@P20,@P21,@P22,\
                            @P23,@P24,@P25,@P26,@P27,@P28,@P29,@P30,@P31,@P32,@P33,\
                            @P34,@P35,@P36,@P37)",
                           &[&id,
                             &data.job_index,
                             &data.part_index,
                             &data.part_lay_l,
                             &data.part_lay_w,
                             &data.part_lay_o,
                             &data.stk_hght_q,
                             &data.stk_hght_d,
                             &data.station,
                             &data.qty_stacks,
                             &data.btm_type,
                             &data.btm_desc,
                             &data.btm_matl,
                             &data.btm_length,
                             &data.btm_width,
                             &data.btm_thick,
                             &data.over_len,
                             &data.over_wid,
                             &data.btm_lay_l,
                             &data.btm_lay_w,
                             &data.top_type,
                             &data.top_desc,
                             &data.top_matl,
                             &data.top_length,
                             &data.top_width,
                             &data.top_thick,
                             &data.top_lay_l,
                             &data.top_lay_w,
                             &data.sup_type,
                             &data.sup_desc,
                             &data.sup_matl,
                             &data.sup_length,
                             &data.sup_width,
                             &data.sup_thick,
                             &data.sup_lay_l,
                             &data.sup_lay_w,
                             &data.station2]).await?;
        }
    }

    //INSERT BOARDS
    for data in &ptx.boards {
        client.execute("INSERT INTO [dbo].[PTX_BOARDS] \
                        ([ID], [JOB_INDEX], [BRD_INDEX], \
                        [CODE], [MAT_INDEX], [LENGTH], [WIDTH], [QTY_STOCK], \
                        [QTY_USED], [COST], [STK_FLAG], [INFORMATION], \
                        [MAT_PARAM], [GRAIN], [TYPE], [BIN], [SUPPLIER]) \
                        VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,\
                        @P12,@P13,@P14,@P15,@P16,@P17)",
                       &[&id,
                         &data.job_index,
                         &data.brd_index,
                         &data.code,
                         &data.mat_index,
                         &data.length,
                         &data.width,
                         &data.qty_stock,
                         &data.qty_used,
                         &data.cost,
                         &data.stk_flag,
                         &data.information,
                         &data.mat_param,
                         &data.grain,
                         &data.r#type,
                         &data.bin,
                         &data.supplier]).await?;
    }

    //INSERT MATERIALS
    for data in &ptx.materials {
        client.execute("INSERT INTO [dbo].[PTX_MATERIALS] \
                        ([ID], [JOB_INDEX], [MAT_INDEX], \
                        [CODE], [DESC], [THICK], [BOOK], [KERF_RIP], \
                        [KERF_XCT], [TRIM_FRIP], [TRIM_VRIP], [TRIM_FXCT], \
                        [TRIM_VXCT], [TRIM_HEAD], [TRIM_FRCT], [TRIM_VRCT], \
                        [RULE1], [RULE2], [RULE3], [RULE4], [MAT_PARAM], \
                        [GRAIN], [PICTURE], [DENSITY]) \
                        VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,\
                        @P12,@P13,@P14,@P15,@P16,@P17,@P18,@P19,@P20,@P21,@P22,\
                        @P23,@P24)",
                       &[&id,
                         &data.job_index,
                         &data.mat_index,
                         &data.code,
                         &data.desc,
                         &data.thick,
                         &data.book,
                         &data.kerf_rip,
                         &data.kerf_xct,
                         &data.trim_frip,
                         &data.trim_vrip,
                         &data.trim_fxct,
                         &data.trim_vxct,
                         &data.trim_head,
                         &data.trim_frct,
                         &data.trim_vrct,
                         &data.rule1,
                         &data.rule2,
                         &data.rule3,
                         &data.rule4,
                         &data.mat_param,
                         &data.grain,
                         &data.picture,
                         &data.density]).await?;
    }

    //INSERT NOTES
    if let Some(vector) = &ptx.notes {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_NOTES] \
                            ([ID], [JOB_INDEX], [NOTES_INDEX], [TEXT]) \
                            VALUES (@P1,@P2,@P3,@P4)",
                           &[&id,
                             &data.job_index,
                             &data.notes_index,
                             &data.text]).await?;
        }
    }

    //INSERT OFFCUTS
    if let Some(vector) = &ptx.offcuts {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_OFFCUTS] \
                            ([ID], [JOB_INDEX], [OFC_INDEX], [CODE] \
                            [MAT_INDEX], [LENGTH], [WIDTH], [OFC_QTY], \
                            [GRAIN], [COST], [TYPE]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11)",
                           &[&id,
                             &data.job_index,
                             &data.ofc_index,
                             &data.code,
                             &data.mat_index,
                             &data.length,
                             &data.width,
                             &data.ofc_qty,
                             &data.grain,
                             &data.cost,
                             &data.r#type]).await?;
        }
    }

    //INSERT PATTERNS
    if let Some(vector) = &ptx.patterns {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_PATTERNS] \
                            ([ID], [JOB_INDEX], [PTN_INDEX], [BRD_INDEX], \
                            [TYPE], [QTY_RUN], [QTY_CYCLES], [MAX_BOOK], \
                            [PICTURE], [CYCLE_TIME], [TOTAL_TIME]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11)",
                           &[&id,
                             &data.job_index,
                             &data.ptn_index,
                             &data.brd_index,
                             &data.r#type,
                             &data.qty_run,
                             &data.qty_cycles,
                             &data.max_book,
                             &data.picture,
                             &data.cycle_time,
                             &data.total_time]).await?;
        }
    }

    //INSERT PTN_UDI
    if let Some(vector) = &ptx.ptn_udi {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_PARTS_UDI] \
                            ([ID], [JOB_INDEX], [PTN_INDEX], \
                            [BRD_INDEX], [STRIP_INDEX], \
                            [INFO01], [INFO02], [INFO03], [INFO04], [INFO05], \
                            [INFO06], [INFO07], [INFO08], [INFO09], [INFO10], \
                            [INFO11], [INFO12], [INFO13], [INFO14], [INFO15], \
                            [INFO16], [INFO17], [INFO18], [INFO19], [INFO20], \
                            [INFO21], [INFO22], [INFO23], [INFO24], [INFO25], \
                            [INFO26], [INFO27], [INFO28], [INFO29], [INFO30], \
                            [INFO31], [INFO32], [INFO33], [INFO34], [INFO35], \
                            [INFO36], [INFO37], [INFO38], [INFO39], [INFO40], \
                            [INFO41], [INFO42], [INFO43], [INFO44], [INFO45], \
                            [INFO46], [INFO47], [INFO48], [INFO49], [INFO50], \
                            [INFO51], [INFO52], [INFO53], [INFO54], [INFO55], \
                            [INFO56], [INFO57], [INFO58], [INFO59], [INFO60], \
                            [INFO61], [INFO62], [INFO63], [INFO64], [INFO65], \
                            [INFO66], [INFO67], [INFO68], [INFO69], [INFO70], \
                            [INFO71], [INFO72], [INFO73], [INFO74], [INFO75], \
                            [INFO76], [INFO77], [INFO78], [INFO79], [INFO80], \
                            [INFO81], [INFO82], [INFO83], [INFO84], [INFO85], \
                            [INFO86], [INFO87], [INFO88], [INFO89], [INFO90], \
                            [INFO91], [INFO92], [INFO93], [INFO94], [INFO95], \
                            [INFO96], [INFO97], [INFO98], [INFO99]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11,\
                            @P12,@P13,@P14,@P15,@P16,@P17,@P18,@P19,@P20,@P21,@P22,\
                            @P23,@P24,@P25,@P26,@P27,@P28,@P29,@P30,@P31,@P32,@P33,\
                            @P34,@P35,@P36,@P37,@P38,@P39,@P40,@P41,@P42,@P43,@P44,\
                            @P45,@P46,@P47,@P48,@P49,@P50,@P51,@P52,@P53,@P54,@P55,\
                            @P56,@P57,@P58,@P59,@P60,@P61,@P62,@P63,@P64,@P65,@P66,\
                            @P67,@P68,@P69,@P70,@P71,@P72,@P73,@P74,@P75,@P76,@P77,\
                            @P78,@P79,@P80,@P81,@P82,@P83,@P84,@P85,@P86,@P87,@P88,\
                            @P89,@P90,@P91,@P92,@P93,@P94,@P95,@P96,@P97,@P98,@P99,\
                            @P100,@P101,@P102,@P103)",
                           &[&id,
                             &data.job_index,
                             &data.ptn_index,
                             &data.brd_index,
                             &data.strip_index,
                             &data.info01,
                             &data.info02,
                             &data.info03,
                             &data.info04,
                             &data.info05,
                             &data.info06,
                             &data.info07,
                             &data.info08,
                             &data.info09,
                             &data.info10,
                             &data.info11,
                             &data.info12,
                             &data.info13,
                             &data.info14,
                             &data.info15,
                             &data.info16,
                             &data.info17,
                             &data.info18,
                             &data.info19,
                             &data.info20,
                             &data.info21,
                             &data.info22,
                             &data.info23,
                             &data.info24,
                             &data.info25,
                             &data.info26,
                             &data.info27,
                             &data.info28,
                             &data.info29,
                             &data.info30,
                             &data.info31,
                             &data.info32,
                             &data.info33,
                             &data.info34,
                             &data.info35,
                             &data.info36,
                             &data.info37,
                             &data.info38,
                             &data.info39,
                             &data.info40,
                             &data.info41,
                             &data.info42,
                             &data.info43,
                             &data.info44,
                             &data.info45,
                             &data.info46,
                             &data.info47,
                             &data.info48,
                             &data.info49,
                             &data.info50,
                             &data.info51,
                             &data.info52,
                             &data.info53,
                             &data.info54,
                             &data.info55,
                             &data.info56,
                             &data.info57,
                             &data.info58,
                             &data.info59,
                             &data.info60,
                             &data.info61,
                             &data.info62,
                             &data.info63,
                             &data.info64,
                             &data.info65,
                             &data.info66,
                             &data.info67,
                             &data.info68,
                             &data.info69,
                             &data.info70,
                             &data.info71,
                             &data.info72,
                             &data.info73,
                             &data.info74,
                             &data.info75,
                             &data.info76,
                             &data.info77,
                             &data.info78,
                             &data.info79,
                             &data.info80,
                             &data.info81,
                             &data.info82,
                             &data.info83,
                             &data.info84,
                             &data.info85,
                             &data.info86,
                             &data.info87,
                             &data.info88,
                             &data.info89,
                             &data.info90,
                             &data.info91,
                             &data.info92,
                             &data.info93,
                             &data.info94,
                             &data.info95,
                             &data.info96,
                             &data.info97,
                             &data.info98,
                             &data.info99]).await?;
        }
    }    

    //INSERT CUTS
    if let Some(vector) = &ptx.cuts {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_CUTS] \
                            ([ID], [JOB_INDEX], [PTN_INDEX], [CUT_INDEX], \
                            [SEQUENCE], [FUNCTION], [DIMENSION], [QTY_RPT], \
                            [PART_INDEX], [QTY_PARTS], [COMMENT]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8,@P9,@P10,@P11)",
                           &[&id,
                             &data.job_index,
                             &data.ptn_index,
                             &data.cut_index,
                             &data.sequence,
                             &data.function,
                             &data.dimension,
                             &data.qty_rpt,
                             &data.part_index,
                             &data.qty_parts,
                             &data.comment]).await?;
        }
    }

    //INSERT VECTORS
    if let Some(vector) = &ptx.vectors {
        for data in vector {
            client.execute("INSERT INTO [dbo].[PTX_VECTORS] \
                            ([ID], [JOB_INDEX], [PTN_INDEX], [CUT_INDEX], \
                            [X_START], [Y_START], [X_END], [Y_END]) \
                            VALUES (@P1,@P2,@P3,@P4,@P5,@P6,@P7,@P8)",
                           &[&id,
                             &data.job_index,
                             &data.ptn_index,
                             &data.cut_index,
                             &data.x_start,
                             &data.y_start,
                             &data.x_end,
                             &data.y_end]).await?;
        }
    }
    
    Ok(())
}
