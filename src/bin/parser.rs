use clap::{Arg, App};
use patternexchange::{Ptx,Cuts,Boards};
use std::error::Error;

use svg::Document;
use svg::node::element::Path;
use svg::node::element::path::Data;

fn main() -> Result<(), Box<dyn Error + Send + Sync>> {
    let matches = App::new("PTXParse")
        .version("0.1.0")
        .author("Jeffrey Stoffers <jstoffers@uberpurple.com>")
        .about("Parser and commandline editor for PTX files")
        .arg(Arg::with_name("file")
             .help("The name of the file/Directory to process.")
             .required(true)
             .index(1))
        .get_matches();

    let file = matches.value_of("file").unwrap();

    let data = Ptx::parse(file);

    // println!("{}", data);

    let pattern = &data.patterns.unwrap()[0];

    let board = &data.boards.iter()
        .filter(|x| x.brd_index == pattern.brd_index && x.job_index == pattern.job_index )
        .collect::<Vec<&Boards>>()[0];
    
    let mut cuts: Vec<Cuts> = Vec::new();

    for x in data.cuts.unwrap() {
        if x.ptn_index == pattern.ptn_index &&
        x.job_index == pattern.job_index {
            cuts.push(x.clone());
        }
    }

    let mut paths: Vec<Path> = Vec::new();

    //let board = (0.0,0.0,board.length,board.width);
    let mut rectangle = (0.0,0.0,board.length,board.width);
    let mut x = 0.0;
    let mut y = 0.0;
    
    for i in &cuts {
        for _ in 0..=i.qty_rpt {
        match i.function {
            // Head Cut
            0 => {
                rectangle = (rectangle.0,rectangle.1,i.dimension,rectangle.3);
                x += i.dimension;
                y = rectangle.1;
                paths.push( line_path(x,y,0.0,rectangle.3,"black") )
            },
            // Rip Cut
            1 => {
                rectangle = (rectangle.0,rectangle.1,rectangle.2,i.dimension);
                y += i.dimension;
                x = rectangle.0;
                paths.push( line_path(x,y,rectangle.2,0.0,"black") );
            },
            // Cross Cut
            2 => {
                rectangle = (rectangle.0,rectangle.1,i.dimension,rectangle.3);
                x += i.dimension;
                y = rectangle.1;
                paths.push( line_path(x,y,0.0,rectangle.3,"black") )
            }
            // Waste Rip Cut
            91 => {
                rectangle = (rectangle.0,rectangle.1,rectangle.2,i.dimension);
                y += i.dimension;
                x = rectangle.0;
                paths.push( line_path(x,y,rectangle.2,0.0,"red") )
            },
            // Waste Cross Cut
            92 => {
                rectangle = (rectangle.0,rectangle.1,i.dimension,rectangle.3);
                x += i.dimension;
                y = rectangle.1;
                paths.push( line_path(x,y,0.0,rectangle.3,"red") )
            }
            _ => {}
        }
        }
    }
    
    let mut document = Document::new()
        .set("viewBox", (0, 0, board.length, board.width));

    for x in paths {
        document = document.add(x);
    }

    for x in cuts {
        println!("{}",x);
    }
    
    svg::save("image.svg", &document).unwrap();
    
    Ok(())
}

pub fn line_path(sx: f64, sy: f64, dx: f64, dy: f64, color: &str) -> Path {
    Path::new()
        .set("fill", "none")
        .set("stroke", color)
        .set("stroke-width", 1)
        .set("d",
             Data::new()
             .move_to((sx, sy))
             .line_by((dx, dy))
             .close()
        )
}
