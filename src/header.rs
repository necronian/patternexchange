use csv::StringRecord;
use std::fmt;

#[derive(Default, Debug, Clone)]
pub struct Header {
    // File version
    pub version: String,
    // File title
    pub title: String,
    // Measurement mode = 0 (metric), 1(decimal inches).
    pub units: i32,
    // This field indicates the origin for the VECTOR drawing records.
    // The origin for the CUT records is assumed to be 0 (top left).
    // 0 = top to bottom - left to right
    // 1 = top to bottom – right to left
    // 2 = bottom to top – left to right
    // 3 = bottom to top – right to left
    pub origin: i32,
    // Indicates whether the waste strip/piece is cut first or last.
    // That is, is the fixed trim done on the leading edge or as a final trim?
    // 0 = trim waste piece first
    // 1 = trim fixed trim first
    pub trim_type: i32,
}

impl Header {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "HEADER" => Header {
                version: r.next().unwrap().trim().to_string(),
                title: r.next().unwrap().trim().to_string(),
                units: r.next().unwrap().trim().parse::<i32>().unwrap(),
                origin: r.next().unwrap().trim().parse::<i32>().unwrap(),
                trim_type: r.next().unwrap().trim().parse::<i32>().unwrap(),
            },
            _ => panic!("Not a header record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.version.to_string(),
            self.title.to_string(),
            self.units.to_string(),
            self.origin.to_string(),
            self.trim_type.to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Header {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "PTX: File - {}\nVersion: {} - Units: {} - Origin: {} - Trim: {}",
               self.title,
               self.version,
               if self.units == 0 { "mm" }
               else if self.units == 1 { "inches" }
               else {"Unknown"},
               self.origin,
               self.trim_type
        )
    }
}
