use csv::StringRecord;
use std::fmt;

// These records define the different material types. There should be a least
// one of these records in data structure. This record is used to pass a
// detailed material description, the thickness and other parameters which may
// vary according to material type.
//
// The saw kerf (saw blade thickness) and trims are optional. Note that it is
// assumed that one of the two rip trims will be pconstant and the other rip
// trim includes the waste strip. Either (a) the leading edge is trimmed and
// the waste strip comes out last, or (b) the waste strip is removed by the
// first rip and the last rip is a constant trim. This assumption also applies
// to cross cut trims and recut trims.
#[derive(Default, Debug, Clone)]
pub struct Materials {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Unique index of material used to link this record to other records
    pub mat_index: i32,
    // Material code
    pub code: String,
    // Material description
    pub desc: String,
    // Material thickness in appropriate measurement mode
    pub thick: f64,
    // Max sheets per book, reflects cutting height of saw
    pub book: i32,
    // Rip saw kerf (saw blade thickness) - in unit of measurement
    pub kerf_rip: f64,
    // Crosscut saw kerf (saw blade thickness) - in unit of measurement
    pub kerf_xct: f64,
    // Fixed rip trim - includes saw kerf (saw blade thickness) - amount sheet
    // size is reduced by
    pub trim_frip: f64,
    // Minimum waste rip trim - minimum size of falling waste including saw
    // kerf (saw blade thickness)
    pub trim_vrip: f64,
    // Fixed crosscut trim - includes saw kerf (saw blade thickness)
    pub trim_fxct: f64,
    // Minimum waste crosscut trim - minimum size of falling waste including
    // saw kerf (saw blade thickness)
    pub trim_vxct: f64,
    // Internal head cut trim - includes saw kerf (saw blade thickness)
    pub trim_head: f64,
    // Fixed recut trim - includes saw kerf (saw blade thickness)
    pub trim_frct: f64,
    // Minimum waste recut trim - minimum size of falling waste including saw
    // kerf (saw blade thickness)
    pub trim_vrct: f64,
    // Optimisation rule 1 - cut nesting limit - 1 to 9 (e.g. 3 = allow third
    // phase recuts)
    pub rule1: i32,
    // Optimisation rule 2 - head cuts allowed (0=No, 1 =Yes)
    pub rule2: i32,
    // Optimisation rule 3 - board rotation allowed (short rip) (0=No, 1=Yes)
    pub rule3: i32,
    // Optimisation rule 4 - show separate patterns for duplicate parts (0=No
    // 1=Yes)
    pub rule4: i32,
    // Material parameters file name
    pub mat_param: String,
    // 0 = No grain,
    // 1 = grain along the length of the board
    // 2 = grain along the width of the board
    pub grain: i32,
    // Solid colour (e.g. "RGB(255:0:0)") or image file (e.g. "Teak.bmp")
    pub picture: String,
    // Material density in metric tons per m3 or pounds per ft3 depending on
    // the current measurement mode.
    pub density: f64,
}

impl Materials {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "MATERIALS" => Materials {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                mat_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                code: r.next().unwrap().trim().to_string(),
                desc: r.next().unwrap().trim().to_string(),
                thick: r.next().unwrap().trim().parse::<f64>().unwrap(),
                book: r.next().unwrap().trim().parse::<i32>().unwrap(),
                kerf_rip: r.next().unwrap().trim().parse::<f64>().unwrap(),
                kerf_xct: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_frip: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_vrip: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_fxct: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_vxct: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_head: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_frct: r.next().unwrap().trim().parse::<f64>().unwrap(),
                trim_vrct: r.next().unwrap().trim().parse::<f64>().unwrap(),
                rule1: r.next().unwrap().trim().parse::<i32>().unwrap(),
                rule2: r.next().unwrap().trim().parse::<i32>().unwrap(),
                rule3: r.next().unwrap().trim().parse::<i32>().unwrap(),
                rule4: r.next().unwrap().trim().parse::<i32>().unwrap(),
                mat_param: r.next().unwrap().trim().to_string(),
                grain: r.next().unwrap().trim().parse::<i32>().unwrap(),
                picture: r.next().unwrap().trim().to_string(),
                density: r.next().unwrap().trim().parse::<f64>().unwrap(),
            },
            _ => panic!("Not a materials record")
        }
    }
    
    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.mat_index.to_string(),
            self.code.to_string(),
            self.desc.to_string(),
            self.thick.to_string(),
            self.book.to_string(),
            self.kerf_rip.to_string(),
            self.kerf_xct.to_string(),
            self.trim_frip.to_string(),
            self.trim_vrip.to_string(),
            self.trim_fxct.to_string(),
            self.trim_vxct.to_string(),
            self.trim_head.to_string(),
            self.trim_frct.to_string(),
            self.trim_vrct.to_string(),
            self.rule1.to_string(),
            self.rule2.to_string(),
            self.rule3.to_string(),
            self.rule4.to_string(),
            self.mat_param.to_string(),
            self.grain.to_string(),
            self.picture.to_string(),
            self.density.to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Materials {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Material - Job IDX: {} - Mat IDX: {}\n\
                Code: {}\n\
                Description: {}\n\
                Thickness: {} - Book Height (sheets): {}\n\
                Kerf Rip/Cross: {}/{}\n\
                Trims: {} {} {} {} {} {} {}\n\
                Nested Cut Limit: {}\n\
                Head Cuts Allowed: {}\n\
                Board Rotation Allowed: {}\n\
                Seperate Duplicate Patterns: {}\n\
                Param: {}\n\
                Grain: {} - Picture: {} - Density: {}",
               self.job_index,
               self.mat_index,
               self.code,
               self.desc,
               self.thick,
               self.book,
               self.kerf_rip,
               self.kerf_xct,
               self.trim_frip,
               self.trim_vrip,
               self.trim_fxct,
               self.trim_vxct,
               self.trim_head,
               self.trim_frct,
               self.trim_vrct,
               self.rule1,
               if self.rule2 == 0 { "False" }
               else if self.rule2 == 1 { "True" }
               else { "Unknown" },
               if self.rule3 == 0 { "False" }
               else if self.rule3 == 1 { "True" }
               else { "Unknown" },
               if self.rule4 == 0 { "False" }
               else if self.rule4 == 1 { "True" }
               else { "Unknown" },
               self.mat_param,
               if self.grain == 0 { "Stock" }
               else if self.grain == 1 { "Offcut" }
               else if self.grain == 2 { "Automatic Offcut" }
               else { "Unknown" },
               self.picture,
               self.density,
        )
    }
}
