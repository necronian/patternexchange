use csv::StringRecord;
use std::fmt;

// This record occurs once per pattern. It is used to describe header detail
// for the pattern, such as board size used, number of sheets to be cut etc.
#[derive(Default, Debug, Clone)]
pub struct Patterns {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Sequential number incrementing by 1 for each pattern record for each
    // job.
    pub ptn_index: i32,
    // Index number from the Boards records.
    pub brd_index: i32,
    // determines the direction of the first cut, and the type of pattern
    // Fixed Pattern
    // 0 = rip length first - non-head cut pattern
    // 1 = turn board before ripping - non-head cut pattern
    // 2 = head cut pattern - head cut across width
    // 3 = head cut pattern - head cut along length
    // 4 = crosscut only
    // Template Pattern
    // 5 = Create master part - divide at saw
    // 6 = Create master part - divide at machining centre
    // 7 = Cut parts in main pattern
    // 8 = Cut parts in separate pattern
    pub r#type: i32,
    // Run quantity - number of sheets to be cut to this pattern
    pub qty_run: i32,
    // Number of cycles or books
    pub qty_cycles: i32,
    // Maximum number of sheets per book (cutting height)
    pub max_book: i32,
    // name of file containing picture of cutting pattern
    pub picture: String,
    // The time in seconds to cut a single cycle
    pub cycle_time: i32,
    // The total time in seconds to cut all cycles
    pub total_time: i32,
}

impl Patterns {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "PATTERNS" => Patterns {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                ptn_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                brd_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                r#type: r.next().unwrap().trim().parse::<i32>().unwrap(),
                qty_run: r.next().unwrap().trim().parse::<i32>().unwrap(),
                qty_cycles: r.next().unwrap().trim().parse::<i32>().unwrap(),
                max_book: r.next().unwrap().trim().parse::<i32>().unwrap(),
                picture: r.next().unwrap().trim().to_string(),
                cycle_time: r.next().unwrap().trim().parse::<i32>().unwrap(),
                total_time: r.next().unwrap().trim().parse::<i32>().unwrap(),
            },
            _ => panic!("Not a patterns record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.ptn_index.to_string(),
            self.brd_index.to_string(),
            self.r#type.to_string(),
            self.qty_run.to_string(),
            self.qty_cycles.to_string(),
            self.max_book.to_string(),
            self.picture.to_string(),
            self.cycle_time.to_string(),
            self.total_time.to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Patterns {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Pattern - Job IDX: {} - Pattern IDX: {} - Board IDX: {}\n\
                Type: {}\n\
                Quanity Run: {} - Cycles: {} - Max Book: {}\n\
                Picture: {}\n\
                Cycle Time: {} minutes - Total Time: {} minutes",
               self.job_index,
               self.ptn_index,
               self.brd_index,
               match self.r#type {
                   0 => "Rip length first",
                   1 => "Turn board first",
                   2 => "Head cut Width",
                   3 => "Head Cut Length",
                   4 => "Crosscut Only",
                   5 => "Create Master Part - Divide at Saw",
                   6 => "Create Master Part - Divide at CNC",
                   7 => "Cut parts in main pattern",
                   8 => "Cut Parts in seperate pattern",
                   _ => "Unknown"
               },
               self.qty_run,
               self.qty_cycles,
               self.max_book,
               self.picture,
               self.cycle_time/60,
               self.total_time/60,
        )
    }
}
