use csv::StringRecord;
use std::fmt;

// These records contain details of the board/sheet sizes to be used; one
// record for each different size/material.
#[derive(Default, Debug, Clone)]
pub struct Boards {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // index number linking this record with the PATTERNS records for this
    // job.
    pub brd_index: i32,
    // Board code - usually the stock code for the sheet size.
    pub code: String,
    // Index of material used for this part.
    pub mat_index: i32,
    // Size of sheet in appropriate measurement unit.
    pub length: f64,
    // Size of sheet in appropriate measurement unit.
    pub width: f64,
    // Total number of sheets available - default 99999 (0=none)
    pub qty_stock: i32,
    // Total number of sheets this size used in patterns
    pub qty_used: i32,
    // Cost per sq. metre or sq. foot according to measurement unit
    pub cost: f64,
    // Flag to indicate action if insufficient stock
    pub stk_flag: i32,
    // Extra descriptive details about the sheet
    pub information: String,
    // Material parameters file name
    pub mat_param: String,
    // 0 = No grain,
    // 1 = grain along the length of the board
    // 2 = grain along the width of the board
    pub grain: i32,
    // 0 = Stock board
    // 1 = Offcut
    // 2 = Automatic offcut
    pub r#type: i32,
    // Board location
    pub bin: String,
    // Board supplier
    pub supplier: String,
}

impl Boards {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "BOARDS" => Boards {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                brd_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                code: r.next().unwrap().trim().to_string(),
                mat_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                length: r.next().unwrap().trim().parse::<f64>().unwrap(),
                width: r.next().unwrap().trim().parse::<f64>().unwrap(),
                qty_stock: r.next().unwrap().trim().parse::<i32>().unwrap(),
                qty_used: r.next().unwrap().trim().parse::<i32>().unwrap(),
                cost: r.next().unwrap().trim().parse::<f64>().unwrap(),
                stk_flag: r.next().unwrap().trim().parse::<i32>().unwrap(),
                information: r.next().unwrap().trim().to_string(),
                mat_param: r.next().unwrap().trim().to_string(),
                grain: r.next().unwrap().trim().parse::<i32>().unwrap(),
                r#type: r.next().unwrap().trim().parse::<i32>().unwrap(),
                bin: r.next().unwrap().trim().to_string(),
                supplier: r.next().unwrap().trim().to_string(),
            },
            _ => panic!("Not a boards record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.brd_index.to_string(),
            self.code.to_string(),
            self.mat_index.to_string(),
            self.length.to_string(),
            self.width.to_string(),
            self.qty_stock.to_string(),
            self.qty_used.to_string(),
            self.cost.to_string(),
            self.stk_flag.to_string(),
            self.information.to_string(),
            self.mat_param.to_string(),
            self.grain.to_string(),
            self.r#type.to_string(),
            self.bin.to_string(),
            self.supplier.to_string(),
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Boards {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Board - Job IDX: {} - Board IDX - {} - Mat IDX: {}\n\
                Code: {}\n\
                Size (LxW): {} x {} - Grain: {}\n\
                Stock: {} - Used: {} Cost: {} - Enough Stock?: {}\n\
                Info: {} - Param: {}\n\
                Type: {} - Supplier: {} - Bin Location: {}",
               self.job_index,
               self.brd_index,
               self.mat_index,
               self.code,
               self.length,
               self.width,
               if self.grain == 0 { "None" }
               else if self.grain == 1 { "Grained" }
               else if self.grain == 2 { "Cross Grained" }
               else { "Unknown" },
               self.qty_stock,
               self.qty_used,
               self.cost,
               if self.stk_flag == 0 { "False"}
               else if self.stk_flag == 1 { "True" }
               else { "Unknown" },
               self.information,
               self.mat_param,
               if self.r#type == 0 { "Stock" }
               else if self.r#type == 1 { "Offcut" }
               else if self.r#type == 2 { "Automatic Offcut" }
               else { "Unknown" },
               self.supplier,
               self.bin,
        )
    }
}
