mod boards;
mod cuts;
mod header;
mod jobs;
mod materials;
mod notes;
mod offcuts;
mod parts_dst;
mod parts_inf;
mod parts_req;
mod parts_udi;
mod patterns;
mod ptn_udi;
mod vectors;

pub use boards::Boards;
pub use cuts::Cuts;
pub use header::Header;
pub use jobs::Jobs;
pub use materials::Materials;
pub use notes::Notes;
pub use offcuts::Offcuts;
pub use parts_dst::PartsDst;
pub use parts_inf::PartsInf;
pub use parts_req::PartsReq;
pub use parts_udi::PartsUdi;
pub use patterns::Patterns;
pub use ptn_udi::PtnUdi;
pub use vectors::Vectors;

use std::fs::File;
use std::path::Path;
use std::fmt;

use csv::ReaderBuilder;

#[derive(Default, Debug, Clone)]
pub struct Ptx {
    pub header: Header,
    pub jobs: Option<Vec<Jobs>>,
    pub parts_req: Vec<PartsReq>,
    pub parts_inf: Option<Vec<PartsInf>>,
    pub parts_udi: Option<Vec<PartsUdi>>,
    pub parts_dst: Option<Vec<PartsDst>>,
    pub boards: Vec<Boards>,
    pub materials: Vec<Materials>,
    pub notes: Option<Vec<Notes>>,
    pub offcuts: Option<Vec<Offcuts>>,
    pub patterns: Option<Vec<Patterns>>,
    pub ptn_udi: Option<Vec<PtnUdi>>,
    pub cuts: Option<Vec<Cuts>>,
    pub vectors: Option<Vec<Vectors>>,
}

impl Ptx {
    pub fn parse<P>(file: P) -> Self
    where P: AsRef<Path>
    {
        let mut header: Header = Header::default();
        let mut jobs: Vec<Jobs> = Vec::new();
        let mut parts_req: Vec<PartsReq> = Vec::new();
        let mut parts_inf: Vec<PartsInf> = Vec::new();
        let mut parts_udi: Vec<PartsUdi> = Vec::new();
        let mut parts_dst: Vec<PartsDst> = Vec::new();
        let mut boards: Vec<Boards> = Vec::new();
        let mut materials: Vec<Materials> = Vec::new();
        let mut notes: Vec<Notes> = Vec::new();
        let mut offcuts: Vec<Offcuts> = Vec::new();
        let mut patterns: Vec<Patterns> = Vec::new();
        let mut ptn_udi: Vec<PtnUdi> = Vec::new();
        let mut cuts: Vec<Cuts> = Vec::new();
        let mut vectors: Vec<Vectors> = Vec::new();

        let file = File::open(file).unwrap();
        let mut rdr = ReaderBuilder::new()
            .has_headers(false)
            .flexible(true)
            .from_reader(file);

        for result in rdr.records() {
            let record = result.unwrap();
            match record.get(0).unwrap() {
                "HEADER" => header = Header::parse_csv(record),
                "JOBS" => jobs.push(Jobs::parse_csv(record)),
                "PARTS_REQ" => parts_req.push(PartsReq::parse_csv(record)),
                "PARTS_INF" => parts_inf.push(PartsInf::parse_csv(record)),
                "PARTS_UDI" => parts_udi.push(PartsUdi::parse_csv(record)),
                "PARTS_DST" => parts_dst.push(PartsDst::parse_csv(record)),
                "BOARDS" => boards.push(Boards::parse_csv(record)),
                "MATERIALS" => materials.push(Materials::parse_csv(record)),
                "NOTES" => notes.push(Notes::parse_csv(record)),
                "OFFCUTS" => offcuts.push(Offcuts::parse_csv(record)),
                "PATTERNS" => patterns.push(Patterns::parse_csv(record)),
                "PTN_UDI" => ptn_udi.push(PtnUdi::parse_csv(record)),
                "CUTS" => cuts.push(Cuts::parse_csv(record)),
                "VECTORS" => vectors.push(Vectors::parse_csv(record)),
                e => panic!("Unmatched Record Type: {}",e)
            }
        }

        Ptx {
            header,
            jobs: if !jobs.is_empty() { Some(jobs) } else { None },
            parts_req,
            parts_inf: if !parts_inf.is_empty() { Some(parts_inf) } else { None },
            parts_udi: if !parts_udi.is_empty() { Some(parts_udi) } else { None },
            parts_dst: if !parts_dst.is_empty() { Some(parts_dst) } else { None },
            boards,
            materials,
            notes: if !notes.is_empty() { Some(notes) } else { None },
            offcuts: if !offcuts.is_empty() { Some(offcuts) } else { None },
            patterns: if !patterns.is_empty() { Some(patterns) } else { None },
            ptn_udi: if !ptn_udi.is_empty() { Some(ptn_udi) } else { None },
            cuts: if !cuts.is_empty() { Some(cuts) } else { None },
            vectors: if !vectors.is_empty() { Some(vectors) } else { None },
        }
    }
}

impl fmt::Display for Ptx {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        write!(f,"{}\n", self.header)?;

        for x in self.jobs.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in &self.parts_req {
            write!(f,"{}\n",x)?;
        }

        for x in self.parts_inf.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.parts_udi.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.parts_dst.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in &self.boards {
            write!(f,"{}\n",x)?;
        }

        for x in &self.materials {
            write!(f,"{}\n",x)?;
        }

        for x in self.notes.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.offcuts.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.patterns.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.ptn_udi.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.cuts.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        for x in self.vectors.as_ref().unwrap_or(&vec![]) {
            write!(f,"{}\n",x)?;
        }

        write!(f,"")
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
