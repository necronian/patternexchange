use csv::StringRecord;
use std::fmt;

// This optional record contains destacking information about each different
// size (or line item) in the cutting list.
#[derive(Default, Debug, Clone)]
pub struct PartsDst {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Index number linking this record with other part records
    pub part_index: i32,
    // Part layout - number of parts per stack in length
    pub part_lay_l: Option<i32>,
    // Part layout - number of parts per stack in width
    pub part_lay_w: Option<i32>,
    // Part layout – orientation
    pub part_lay_o: Option<i32>,
    // Stack height – quantity of pieces
    pub stk_hght_q: Option<i32>,
    // Stack height – dimension
    pub stk_hght_d: Option<i32>,
    // Station number
    pub station: Option<i32>,
    // Total number of stacks (pallets) for this part
    pub qty_stacks: Option<i32>,
    // Bottom destacking type
    pub btm_type: Option<i32>,
    // Bottom description
    pub btm_desc: Option<String>,
    // Bottom baseboard material
    pub btm_matl: Option<String>,
    // Length of bottom baseboard/pallet
    pub btm_length: Option<f64>,
    // Width of bottom baseboard/pallet
    pub btm_width: Option<f64>,
    //Thickness of bottom baseboard/pallet
    pub btm_thick: Option<f64>,
    // Overhang/oversize per side in length
    pub over_len: Option<f64>,
    // Overhang/oversize per side in width
    pub over_wid: Option<f64>,
    // Layout of bottom baseboards/pallets in station in length
    pub btm_lay_l: Option<i32>,
    // Layout of bottom baseboards/pallets in station in width
    pub btm_lay_w: Option<i32>,
    // Top cover type
    pub top_type: Option<i32>,
    // Top baseboard/cover description
    pub top_desc: Option<String>,
    // Top baseboard material
    pub top_matl: Option<String>,
    // Length of top baseboard/cover
    pub top_length: Option<f64>,
    // Width of top baseboard/cover
    pub top_width: Option<f64>,
    //Thickness of top baseboard/cover
    pub top_thick: Option<f64>,
    // Layout of top baseboards in length
    pub top_lay_l: Option<i32>,
    // Layout of top baseboards in width
    pub top_lay_w: Option<i32>,
    // Support type
    pub sup_type: Option<i32>,
    // Support description
    pub sup_desc: Option<String>,
    // Support material
    pub sup_matl: Option<String>,
    // Length of support
    pub sup_length: Option<f64>,
    // Width of support
    pub sup_width: Option<f64>,
    // Thickness of support
    pub sup_thick: Option<f64>,
    // Support layout in length
    pub sup_lay_l: Option<i32>,
    // Support layout in width
    pub sup_lay_w: Option<i32>,
    // Alternative station number
    pub station2: Option<i32>,
}

impl PartsDst {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "PARTS_DST" => PartsDst {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                part_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                part_lay_l: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                part_lay_w: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                part_lay_o: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                stk_hght_q: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                stk_hght_d: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                station: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                qty_stacks: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                btm_type: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                btm_desc: r.next()
                    .map(|x| Some(x.to_string()))
                    .unwrap_or(None), 
                btm_matl: r.next()
                    .map(|x| Some(x.to_string()))
                    .unwrap_or(None), 
                btm_length: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                btm_width: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                btm_thick: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                over_len: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                over_wid: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                btm_lay_l: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                btm_lay_w: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                top_type: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                top_desc: r.next()
                    .map(|x| Some(x.to_string()))
                    .unwrap_or(None), 
                top_matl: r.next()
                    .map(|x| Some(x.to_string()))
                    .unwrap_or(None), 
                top_length: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                top_width: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                top_thick: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                top_lay_l: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                top_lay_w: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                sup_type: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                sup_desc: r.next()
                    .map(|x| Some(x.to_string()))
                    .unwrap_or(None), 
                sup_matl: r.next()
                    .map(|x| Some(x.to_string()))
                    .unwrap_or(None), 
                sup_length: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                sup_width: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                sup_thick: r.next()
                    .map(|x| Some(x.parse::<f64>().unwrap()))
                    .unwrap_or(None), 
                sup_lay_l: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                sup_lay_w: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
                station2: r.next()
                    .map(|x| Some(x.parse::<i32>().unwrap()))
                    .unwrap_or(None), 
            },
            _ => panic!("Not a jobs record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.part_index.to_string(),
            if let Some(data) = self.part_lay_l { data.to_string() } else { "".to_string() },
            if let Some(data) = self.part_lay_w { data.to_string() } else { "".to_string() },
            if let Some(data) = self.part_lay_o { data.to_string() } else { "".to_string() },
            if let Some(data) = self.stk_hght_q { data.to_string() } else { "".to_string() },
            if let Some(data) = self.stk_hght_d { data.to_string() } else { "".to_string() },
            if let Some(data) = self.station { data.to_string() } else { "".to_string() },
            if let Some(data) = self.qty_stacks { data.to_string() } else { "".to_string() },
            if let Some(data) = self.btm_type { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.btm_desc { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.btm_matl { data.to_string() } else { "".to_string() },
            if let Some(data) = self.btm_length { data.to_string() } else { "".to_string() },
            if let Some(data) = self.btm_width { data.to_string() } else { "".to_string() },
            if let Some(data) = self.btm_thick { data.to_string() } else { "".to_string() },
            if let Some(data) = self.over_len { data.to_string() } else { "".to_string() },
            if let Some(data) = self.over_wid { data.to_string() } else { "".to_string() },
            if let Some(data) = self.btm_lay_l { data.to_string() } else { "".to_string() },
            if let Some(data) = self.btm_lay_w { data.to_string() } else { "".to_string() },
            if let Some(data) = self.top_type { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.top_desc { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.top_matl { data.to_string() } else { "".to_string() },
            if let Some(data) = self.top_length { data.to_string() } else { "".to_string() },
            if let Some(data) = self.top_width { data.to_string() } else { "".to_string() },
            if let Some(data) = self.top_thick { data.to_string() } else { "".to_string() },
            if let Some(data) = self.top_lay_l { data.to_string() } else { "".to_string() },
            if let Some(data) = self.top_lay_w { data.to_string() } else { "".to_string() },
            if let Some(data) = self.sup_type { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.sup_desc { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.sup_matl { data.to_string() } else { "".to_string() },
            if let Some(data) = self.sup_length { data.to_string() } else { "".to_string() },
            if let Some(data) = self.sup_width { data.to_string() } else { "".to_string() },
            if let Some(data) = self.sup_thick { data.to_string() } else { "".to_string() },
            if let Some(data) = self.sup_lay_l { data.to_string() } else { "".to_string() },
            if let Some(data) = self.sup_lay_w { data.to_string() } else { "".to_string() },
            if let Some(data) = self.station2 { data.to_string() } else { "".to_string() },
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for PartsDst {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "",
        )
    }
}
