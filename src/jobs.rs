use csv::StringRecord;
use std::fmt;

// This record contains data about each job contained in the file. These
// records are optional and in the absence of job records all parts and
// patterns are assumed to belong to the same job.
#[derive(Default, Debug, Clone)]
pub struct Jobs {
    // Unique index number used to link other records to an appropriate job
    pub job_index: i32,
    // Job number/name – reference for job
    pub name: String,
    // Job description/title - title of job
    pub desc: String,
    // Date of order (DD/MM/YYYY)
    pub ord_date: String,
    // Date for cutting/delivery (DD/MM/YYYY)
    pub cut_date: String,
    // Customer code or name
    pub customer: String,
    // Status of the job.
    // 0 - not optimised
    // 1 - optimised
    // 2 - optimise failed
    // Note: there may be a range of other error codes
    pub status: i32,
    // Optimising parameter file name
    pub opt_param: String,
    // Saw parameter file name
    pub saw_param: String,
    // Total cutting time for the job in seconds
    pub cut_time: Option<i32>,
    // Overall percentage waste as a percentage of board area
    pub waste_pcnt: Option<f64>,
}

impl Jobs {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "JOBS" => Jobs {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                name: r.next().unwrap().trim().to_string(),
                desc: r.next().unwrap().trim().to_string(),
                ord_date: r.next().unwrap().trim().to_string(),
                cut_date: r.next().unwrap().trim().to_string(),
                customer: r.next().unwrap().trim().to_string(),
                status: r.next().unwrap().trim().parse::<i32>().unwrap(),
                opt_param: r.next().unwrap().trim().to_string(),
                saw_param: r.next().unwrap().trim().to_string(),
                cut_time: r.next()
                    .map(|x| Some(x.trim().parse::<i32>().unwrap()))
                    .unwrap_or(None),
                waste_pcnt: r.next()
                    .map(|x| Some(x.trim().parse::<f64>().unwrap()))
                    .unwrap_or(None),
            },
            _ => panic!("Not a jobs record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.name.to_string(),
            self.desc.to_string(),
            self.ord_date.to_string(),
            self.cut_date.to_string(),
            self.customer.to_string(),
            self.status.to_string(),
            self.opt_param.to_string(),
            self.saw_param.to_string(),
            if let Some(data) = self.cut_time { data.to_string() } else { "".to_string() },
            if let Some(data) = self.waste_pcnt { data.to_string() } else { "".to_string() },
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Jobs {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Job IDX: {} - Name: {} - Customer: {}\nDesc: {}\nOrdered: {} Due: {}\nStatus: {} - Optimization: {} - Saw: {}\nTime: {} Minutes - Waste: {}%",
               self.job_index,
               self.name,
               self.customer,
               self.desc,
               self.ord_date,
               self.cut_date,
               self.status,
               self.opt_param,
               self.saw_param,
               if let Some(data) = self.cut_time { (data/60).to_string() } else { "".to_string() },
               if let Some(data) = self.waste_pcnt { data.to_string() } else { "".to_string() },
        )
    }
}
