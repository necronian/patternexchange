use csv::StringRecord;
use std::fmt;

// This record is optional and can occur once for each different off-cut size
// per material created by the cutting patterns.
#[derive(Default, Debug, Clone)]
pub struct Offcuts {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Unique index number of offcut used to link this record to the CUT
    // record.
    pub ofc_index: i32,
    // Offcut code or description - used to identify offcut.
    pub code: String,
    // Index of material used for this offcut. Enables offcuts of similar
    // material composition, thickness and colour, but different size to be
    // grouped together.
    pub mat_index: i32,
    // Length of offcut in appropriate measurement unit
    pub length: f64,
    // Width of offcut in appropriate measurement unit
    pub width: f64,
    // Quantity of this size produced
    pub ofc_qty: i32,
    // 0 = No grain, 1 = grain along the length of the offcut 2 = grain along
    // the width of the offcut
    pub grain: i32,
    // Cost per sq. metre or sq. foot according to measurement unit
    pub cost: Option<f64>,
    // 1 = Offcut 2 = Automatic offcut
    pub r#type: Option<i32>,
}

impl Offcuts {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "OFFCUTS" => Offcuts {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                ofc_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                code: r.next().unwrap().trim().to_string(),
                mat_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                length: r.next().unwrap().trim().parse::<f64>().unwrap(),
                width: r.next().unwrap().trim().parse::<f64>().unwrap(),
                ofc_qty: r.next().unwrap().trim().parse::<i32>().unwrap(),
                grain: r.next().unwrap().trim().parse::<i32>().unwrap(),
                cost: r.next()
                    .map(|x| Some(x.trim().parse::<f64>().unwrap()))
                    .unwrap_or(None),
                r#type: r.next()
                    .map(|x| Some(x.trim().parse::<i32>().unwrap()))
                    .unwrap_or(None),
            },
            _ => panic!("Not an offcuts record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.ofc_index.to_string(),
            self.code.to_string(),
            self.mat_index.to_string(),
            self.length.to_string(),
            self.width.to_string(),
            self.ofc_qty.to_string(),
            self.grain.to_string(),
            if let Some(data) = self.cost { data.to_string() } else { "".to_string() },
            if let Some(data) = self.r#type { data.to_string() } else { "".to_string() },
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for Offcuts {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Offcut - Job IDX: {} - Offcut IDX: {} - Mat IDX: {}\n\
                Code: {}\n\
                Size (LxW): {} x {} - Quantity - {} - Grain: {}\n\
                Cost: {} - Type: {}",
               self.job_index,
               self.ofc_index,
               self.mat_index,
               self.code,
               self.length,
               self.width,
               self.ofc_qty,
               if self.grain == 0 { "Stock" }
               else if self.grain == 1 { "Offcut" }
               else if self.grain == 2 { "Automatic Offcut" }
               else { "Unknown" },
               if let Some(data) = self.cost { data.to_string() } else { "".to_string() },
               if self.r#type == Some(1) { "Offcut" }
               else if self.r#type == Some(2) { "Automatic Offcut" }
               else { "Unknown" },
        )
    }
}
