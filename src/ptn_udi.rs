use csv::StringRecord;
use std::fmt;

// These records are used to indicate the matching information used when
// inserting parts in a strip. This record only applies when all parts in the
// strip must have the same information.
#[derive(Default, Debug, Clone)]
pub struct PtnUdi {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Sequential number incrementing by 1 for each pattern record for each
    // job.
    pub ptn_index: i32,
    // Index number from the Boards records.
    pub brd_index: i32,
    // Strip number (top to bottom, left to right).
    pub strip_index: i32,
    // Info fields for matching parts in a strip
    pub info01: Option<String>,
    pub info02: Option<String>,
    pub info03: Option<String>,
    pub info04: Option<String>,
    pub info05: Option<String>,
    pub info06: Option<String>,
    pub info07: Option<String>,
    pub info08: Option<String>,
    pub info09: Option<String>,
    pub info10: Option<String>,
    pub info11: Option<String>,
    pub info12: Option<String>,
    pub info13: Option<String>,
    pub info14: Option<String>,
    pub info15: Option<String>,
    pub info16: Option<String>,
    pub info17: Option<String>,
    pub info18: Option<String>,
    pub info19: Option<String>,
    pub info20: Option<String>,
    pub info21: Option<String>,
    pub info22: Option<String>,
    pub info23: Option<String>,
    pub info24: Option<String>,
    pub info25: Option<String>,
    pub info26: Option<String>,
    pub info27: Option<String>,
    pub info28: Option<String>,
    pub info29: Option<String>,
    pub info30: Option<String>,
    pub info31: Option<String>,
    pub info32: Option<String>,
    pub info33: Option<String>,
    pub info34: Option<String>,
    pub info35: Option<String>,
    pub info36: Option<String>,
    pub info37: Option<String>,
    pub info38: Option<String>,
    pub info39: Option<String>,
    pub info40: Option<String>,
    pub info41: Option<String>,
    pub info42: Option<String>,
    pub info43: Option<String>,
    pub info44: Option<String>,
    pub info45: Option<String>,
    pub info46: Option<String>,
    pub info47: Option<String>,
    pub info48: Option<String>,
    pub info49: Option<String>,
    pub info50: Option<String>,
    pub info51: Option<String>,
    pub info52: Option<String>,
    pub info53: Option<String>,
    pub info54: Option<String>,
    pub info55: Option<String>,
    pub info56: Option<String>,
    pub info57: Option<String>,
    pub info58: Option<String>,
    pub info59: Option<String>,
    pub info60: Option<String>,
    pub info61: Option<String>,
    pub info62: Option<String>,
    pub info63: Option<String>,
    pub info64: Option<String>,
    pub info65: Option<String>,
    pub info66: Option<String>,
    pub info67: Option<String>,
    pub info68: Option<String>,
    pub info69: Option<String>,
    pub info70: Option<String>,
    pub info71: Option<String>,
    pub info72: Option<String>,
    pub info73: Option<String>,
    pub info74: Option<String>,
    pub info75: Option<String>,
    pub info76: Option<String>,
    pub info77: Option<String>,
    pub info78: Option<String>,
    pub info79: Option<String>,
    pub info80: Option<String>,
    pub info81: Option<String>,
    pub info82: Option<String>,
    pub info83: Option<String>,
    pub info84: Option<String>,
    pub info85: Option<String>,
    pub info86: Option<String>,
    pub info87: Option<String>,
    pub info88: Option<String>,
    pub info89: Option<String>,
    pub info90: Option<String>,
    pub info91: Option<String>,
    pub info92: Option<String>,
    pub info93: Option<String>,
    pub info94: Option<String>,
    pub info95: Option<String>,
    pub info96: Option<String>,
    pub info97: Option<String>,
    pub info98: Option<String>,
    pub info99: Option<String>,
}

impl PtnUdi {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "PTN_UDI" => PtnUdi {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                ptn_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                brd_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                strip_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                info01: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info02: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info03: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info04: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info05: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info06: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info07: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info08: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info09: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info10: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info11: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info12: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info13: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info14: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info15: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info16: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info17: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info18: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info19: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info20: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info21: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info22: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info23: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info24: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info25: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info26: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info27: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info28: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info29: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info30: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info31: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info32: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info33: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info34: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info35: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info36: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info37: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info38: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info39: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info40: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info41: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info42: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info43: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info44: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info45: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info46: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info47: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info48: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info49: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info50: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info51: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info52: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info53: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info54: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info55: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info56: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info57: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info58: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info59: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
                info60: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info61: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info62: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info63: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info64: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info65: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info66: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info67: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info68: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info69: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info70: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info71: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info72: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info73: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info74: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info75: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info76: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info77: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info78: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info79: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info80: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info81: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info82: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info83: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info84: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info85: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info86: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info87: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info88: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info89: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info90: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info91: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info92: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info93: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info94: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info95: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info96: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info97: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info98: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info99: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
            },
            _ => panic!("Not a ptn_udi record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.ptn_index.to_string(),
            self.brd_index.to_string(),
            self.strip_index.to_string(),
            if let Some(data) = &self.info01 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info02 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info03 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info04 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info05 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info06 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info07 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info08 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info09 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info10 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info11 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info12 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info13 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info14 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info15 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info16 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info17 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info18 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info19 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info20 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info21 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info22 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info23 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info24 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info25 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info26 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info27 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info28 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info29 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info30 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info31 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info32 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info33 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info34 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info35 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info36 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info37 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info38 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info39 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info40 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info41 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info42 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info43 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info44 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info45 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info46 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info47 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info48 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info49 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info50 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info51 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info52 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info53 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info54 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info55 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info56 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info57 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info58 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info59 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info60 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info61 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info62 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info63 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info64 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info65 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info66 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info67 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info68 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info69 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info70 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info71 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info72 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info73 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info74 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info75 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info76 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info77 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info78 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info79 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info80 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info81 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info82 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info83 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info84 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info85 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info86 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info87 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info88 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info89 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info90 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info91 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info92 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info93 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info94 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info95 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info96 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info97 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info98 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info99 { data.to_string() } else { "".to_string() },
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for PtnUdi {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f,
               "Pattern Info - Job IDX: {} - Pattern IDX: {} - Board IDX: {} - Strip IDX: {}\n\
                Info01: {}\n\
                Info02: {}\n\
                Info03: {}\n\
                Info04: {}\n\
                Info05: {}\n\
                Info06: {}\n\
                Info07: {}\n\
                Info08: {}\n\
                Info09: {}\n\
                Info10: {}\n\
                Info11: {}\n\
                Info12: {}\n\
                Info13: {}\n\
                Info14: {}\n\
                Info15: {}\n\
                Info16: {}\n\
                Info17: {}\n\
                Info18: {}\n\
                Info19: {}\n\
                Info20: {}\n\
                Info21: {}\n\
                Info22: {}\n\
                Info23: {}\n\
                Info24: {}\n\
                Info25: {}\n\
                Info26: {}\n\
                Info27: {}\n\
                Info28: {}\n\
                Info29: {}\n\
                Info30: {}\n\
                Info31: {}\n\
                Info32: {}\n\
                Info33: {}\n\
                Info34: {}\n\
                Info35: {}\n\
                Info36: {}\n\
                Info37: {}\n\
                Info38: {}\n\
                Info39: {}\n\
                Info40: {}\n\
                Info41: {}\n\
                Info42: {}\n\
                Info43: {}\n\
                Info44: {}\n\
                Info45: {}\n\
                Info46: {}\n\
                Info47: {}\n\
                Info48: {}\n\
                Info49: {}\n\
                Info50: {}\n\
                Info51: {}\n\
                Info52: {}\n\
                Info53: {}\n\
                Info54: {}\n\
                Info55: {}\n\
                Info56: {}\n\
                Info57: {}\n\
                Info58: {}\n\
                Info59: {}\n\
                Info60: {}\n\
                Info61: {}\n\
                Info62: {}\n\
                Info63: {}\n\
                Info64: {}\n\
                Info65: {}\n\
                Info66: {}\n\
                Info67: {}\n\
                Info68: {}\n\
                Info69: {}\n\
                Info70: {}\n\
                Info71: {}\n\
                Info72: {}\n\
                Info73: {}\n\
                Info74: {}\n\
                Info75: {}\n\
                Info76: {}\n\
                Info77: {}\n\
                Info78: {}\n\
                Info79: {}\n\
                Info80: {}\n\
                Info81: {}\n\
                Info82: {}\n\
                Info83: {}\n\
                Info84: {}\n\
                Info85: {}\n\
                Info86: {}\n\
                Info87: {}\n\
                Info88: {}\n\
                Info89: {}\n\
                Info90: {}\n\
                Info91: {}\n\
                Info92: {}\n\
                Info93: {}\n\
                Info94: {}\n\
                Info95: {}\n\
                Info96: {}\n\
                Info97: {}\n\
                Info98: {}\n\
                Info99: {}",
               self.job_index,
               self.ptn_index,
               self.brd_index,
               self.strip_index,
               if let Some(data) = &self.info01 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info02 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info03 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info04 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info05 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info06 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info07 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info08 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info09 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info10 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info11 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info12 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info13 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info14 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info15 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info16 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info17 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info18 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info19 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info20 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info21 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info22 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info23 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info24 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info25 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info26 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info27 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info28 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info29 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info30 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info31 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info32 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info33 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info34 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info35 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info36 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info37 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info38 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info39 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info40 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info41 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info42 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info43 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info44 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info45 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info46 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info47 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info48 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info49 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info50 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info51 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info52 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info53 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info54 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info55 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info56 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info57 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info58 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info59 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info60 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info61 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info62 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info63 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info64 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info65 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info66 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info67 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info68 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info69 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info70 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info71 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info72 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info73 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info74 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info75 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info76 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info77 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info78 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info79 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info80 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info81 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info82 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info83 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info84 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info85 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info86 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info87 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info88 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info89 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info90 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info91 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info92 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info93 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info94 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info95 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info96 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info97 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info98 { data.to_string() } else { "".to_string() },
               if let Some(data) = &self.info99 { data.to_string() } else { "".to_string() },
        )
    }
}
