use csv::StringRecord;
use std::fmt;

// This optional record contains user-defined information about each different
// size (or line item) in the cutting list. One use of this data is for label
// printing.
#[derive(Default, Debug, Clone)]
pub struct PartsUdi {
    // Index number used to link this record to other records for this job.
    pub job_index: i32,
    // Index number linking this record with other part records
    pub part_index: i32,
    // There are up to 60 information fields. The name of each field is INFO
    // followed by the field number. The fields may be used for any purpose
    // such as customer details, dates, CNC operations, and other items not
    // included in the other part records
    pub info01: Option<String>,
    pub info02: Option<String>,
    pub info03: Option<String>,
    pub info04: Option<String>,
    pub info05: Option<String>,
    pub info06: Option<String>,
    pub info07: Option<String>,
    pub info08: Option<String>,
    pub info09: Option<String>,
    pub info10: Option<String>,
    pub info11: Option<String>,
    pub info12: Option<String>,
    pub info13: Option<String>,
    pub info14: Option<String>,
    pub info15: Option<String>,
    pub info16: Option<String>,
    pub info17: Option<String>,
    pub info18: Option<String>,
    pub info19: Option<String>,
    pub info20: Option<String>,
    pub info21: Option<String>,
    pub info22: Option<String>,
    pub info23: Option<String>,
    pub info24: Option<String>,
    pub info25: Option<String>,
    pub info26: Option<String>,
    pub info27: Option<String>,
    pub info28: Option<String>,
    pub info29: Option<String>,
    pub info30: Option<String>,
    pub info31: Option<String>,
    pub info32: Option<String>,
    pub info33: Option<String>,
    pub info34: Option<String>,
    pub info35: Option<String>,
    pub info36: Option<String>,
    pub info37: Option<String>,
    pub info38: Option<String>,
    pub info39: Option<String>,
    pub info40: Option<String>,
    pub info41: Option<String>,
    pub info42: Option<String>,
    pub info43: Option<String>,
    pub info44: Option<String>,
    pub info45: Option<String>,
    pub info46: Option<String>,
    pub info47: Option<String>,
    pub info48: Option<String>,
    pub info49: Option<String>,
    pub info50: Option<String>,
    pub info51: Option<String>,
    pub info52: Option<String>,
    pub info53: Option<String>,
    pub info54: Option<String>,
    pub info55: Option<String>,
    pub info56: Option<String>,
    pub info57: Option<String>,
    pub info58: Option<String>,
    pub info59: Option<String>,
    pub info60: Option<String>,
}

impl PartsUdi {
    pub fn parse_csv(record: StringRecord) -> Self
    {
        let mut r = record.iter();
        match r.next().expect("No Record Found!") {
            "PARTS_UDI" => PartsUdi {
                job_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                part_index: r.next().unwrap().trim().parse::<i32>().unwrap(),
                info01: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info02: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info03: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info04: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info05: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info06: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info07: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info08: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info09: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info10: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info11: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info12: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info13: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info14: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info15: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info16: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info17: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info18: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info19: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info20: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info21: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info22: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info23: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info24: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info25: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info26: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info27: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info28: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info29: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info30: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info31: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info32: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info33: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info34: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info35: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info36: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info37: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info38: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info39: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info40: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info41: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info42: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info43: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info44: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info45: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info46: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info47: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info48: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info49: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info50: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info51: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info52: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info53: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info54: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info55: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info56: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info57: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info58: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info59: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None),
                info60: r.next()
                    .map(|x| Some(x.trim().to_string()))
                    .unwrap_or(None), 
            },
            _ => panic!("Not a parts_udi record")
        }
    }

    pub fn to_csv(&self) -> String {
        let mut wtr = csv::WriterBuilder::new()
            .from_writer(vec![]);

        let data = [
            self.job_index.to_string(),
            self.part_index.to_string(),
            if let Some(data) = &self.info01 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info02 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info03 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info04 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info05 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info06 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info07 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info08 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info09 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info10 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info11 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info12 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info13 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info14 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info15 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info16 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info17 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info18 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info19 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info20 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info21 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info22 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info23 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info24 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info25 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info26 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info27 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info28 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info29 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info30 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info31 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info32 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info33 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info34 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info35 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info36 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info37 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info38 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info39 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info40 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info41 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info42 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info43 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info44 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info45 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info46 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info47 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info48 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info49 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info50 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info51 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info52 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info53 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info54 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info55 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info56 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info57 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info58 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info59 { data.to_string() } else { "".to_string() },
            if let Some(data) = &self.info60 { data.to_string() } else { "".to_string() },
        ];
        
        wtr.write_record(&data)
            .expect("Unable to write csv line.");

        String::from_utf8(wtr.into_inner()
                          .expect("Unable to return csv writer contents"))
            .expect("Unable to convert to utf8 String")
    }
}

impl fmt::Display for PartsUdi {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        //info01: Option<String>,
        write!(f,
               "Part Info - Job IDX: {} - Part IDX: {}\n\
                Info 1: {}\n\
                Info 2: {}\n\
                Info 3: {}\n\
                Info 4: {}\n\
                Info 5: {}\n\
                Info 6: {}\n\
                Info 7: {}\n\
                Info 8: {}\n\
                Info 9: {}\n\
                Info 10: {}\n\
                Info 11: {}\n\
                Info 12: {}\n\
                Info 13: {}\n\
                Info 14: {}\n\
                Info 15: {}\n\
                Info 16: {}\n\
                Info 17: {}\n\
                Info 18: {}\n\
                Info 19: {}\n\
                Info 20: {}\n\
                Info 21: {}\n\
                Info 22: {}\n\
                Info 23: {}\n\
                Info 24: {}\n\
                Info 25: {}\n\
                Info 26: {}\n\
                Info 27: {}\n\
                Info 28: {}\n\
                Info 29: {}\n\
                Info 30: {}\n\
                Info 31: {}\n\
                Info 32: {}\n\
                Info 33: {}\n\
                Info 34: {}\n\
                Info 35: {}\n\
                Info 36: {}\n\
                Info 37: {}\n\
                Info 38: {}\n\
                Info 39: {}\n\
                Info 40: {}\n\
                Info 41: {}\n\
                Info 42: {}\n\
                Info 43: {}\n\
                Info 44: {}\n\
                Info 45: {}\n\
                Info 46: {}\n\
                Info 47: {}\n\
                Info 48: {}\n\
                Info 49: {}\n\
                Info 50: {}\n\
                Info 51: {}\n\
                Info 52: {}\n\
                Info 53: {}\n\
                Info 54: {}\n\
                Info 55: {}\n\
                Info 56: {}\n\
                Info 57: {}\n\
                Info 58: {}\n\
                Info 59: {}\n\
                Info 60: {}",
               self.job_index,
               self.part_index,
               self.info01.as_ref().unwrap_or(&"".to_string()),
               self.info02.as_ref().unwrap_or(&"".to_string()),
               self.info03.as_ref().unwrap_or(&"".to_string()),
               self.info04.as_ref().unwrap_or(&"".to_string()),
               self.info05.as_ref().unwrap_or(&"".to_string()),
               self.info06.as_ref().unwrap_or(&"".to_string()),
               self.info07.as_ref().unwrap_or(&"".to_string()),
               self.info08.as_ref().unwrap_or(&"".to_string()),
               self.info09.as_ref().unwrap_or(&"".to_string()),
               self.info10.as_ref().unwrap_or(&"".to_string()),
               self.info11.as_ref().unwrap_or(&"".to_string()),
               self.info12.as_ref().unwrap_or(&"".to_string()),
               self.info13.as_ref().unwrap_or(&"".to_string()),
               self.info14.as_ref().unwrap_or(&"".to_string()),
               self.info15.as_ref().unwrap_or(&"".to_string()),
               self.info16.as_ref().unwrap_or(&"".to_string()),
               self.info17.as_ref().unwrap_or(&"".to_string()),
               self.info18.as_ref().unwrap_or(&"".to_string()),
               self.info19.as_ref().unwrap_or(&"".to_string()),
               self.info20.as_ref().unwrap_or(&"".to_string()),
               self.info21.as_ref().unwrap_or(&"".to_string()),
               self.info22.as_ref().unwrap_or(&"".to_string()),
               self.info23.as_ref().unwrap_or(&"".to_string()),
               self.info24.as_ref().unwrap_or(&"".to_string()),
               self.info25.as_ref().unwrap_or(&"".to_string()),
               self.info26.as_ref().unwrap_or(&"".to_string()),
               self.info27.as_ref().unwrap_or(&"".to_string()),
               self.info28.as_ref().unwrap_or(&"".to_string()),
               self.info29.as_ref().unwrap_or(&"".to_string()),
               self.info30.as_ref().unwrap_or(&"".to_string()),
               self.info31.as_ref().unwrap_or(&"".to_string()),
               self.info32.as_ref().unwrap_or(&"".to_string()),
               self.info33.as_ref().unwrap_or(&"".to_string()),
               self.info34.as_ref().unwrap_or(&"".to_string()),
               self.info35.as_ref().unwrap_or(&"".to_string()),
               self.info36.as_ref().unwrap_or(&"".to_string()),
               self.info37.as_ref().unwrap_or(&"".to_string()),
               self.info38.as_ref().unwrap_or(&"".to_string()),
               self.info39.as_ref().unwrap_or(&"".to_string()),
               self.info40.as_ref().unwrap_or(&"".to_string()),
               self.info41.as_ref().unwrap_or(&"".to_string()),
               self.info42.as_ref().unwrap_or(&"".to_string()),
               self.info43.as_ref().unwrap_or(&"".to_string()),
               self.info44.as_ref().unwrap_or(&"".to_string()),
               self.info45.as_ref().unwrap_or(&"".to_string()),
               self.info46.as_ref().unwrap_or(&"".to_string()),
               self.info47.as_ref().unwrap_or(&"".to_string()),
               self.info48.as_ref().unwrap_or(&"".to_string()),
               self.info49.as_ref().unwrap_or(&"".to_string()),
               self.info50.as_ref().unwrap_or(&"".to_string()),
               self.info51.as_ref().unwrap_or(&"".to_string()),
               self.info52.as_ref().unwrap_or(&"".to_string()),
               self.info53.as_ref().unwrap_or(&"".to_string()),
               self.info54.as_ref().unwrap_or(&"".to_string()),
               self.info55.as_ref().unwrap_or(&"".to_string()),
               self.info56.as_ref().unwrap_or(&"".to_string()),
               self.info57.as_ref().unwrap_or(&"".to_string()),
               self.info58.as_ref().unwrap_or(&"".to_string()),
               self.info59.as_ref().unwrap_or(&"".to_string()),
               self.info60.as_ref().unwrap_or(&"".to_string()),
        )
    }
}
